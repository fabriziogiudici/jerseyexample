package org.jerseyexample.api;

import org.jerseyexample.api.core.DataManager;
import org.jerseyexample.model.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by warez on 24/04/17.
 */
@Component
public class UserManager {

    @Autowired
    private DataManager dataManager;

    public UserVO login(String userId, String password) throws Exception {
        UserVO user = dataManager.login(userId, password);
        return user;
    }

    public UserVO logout(String token) throws Exception {
        UserVO user = dataManager.logout(token);
        return user;
    }
}
