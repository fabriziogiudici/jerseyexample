package org.jerseyexample.api;

import org.jerseyexample.api.core.DataManager;
import org.jerseyexample.model.MessageLVO;
import org.jerseyexample.model.MessageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by warez on 24/04/17.
 */
@Component
public class MessageManager {

    @Autowired
    private DataManager dataManager;

    public MessageVO sendMessage(String token, MessageVO message) throws Exception {
        MessageVO m = dataManager.sendMessage(token ,message);
        return m;
    }

    public <T extends MessageLVO> T get(String token, String messageId, boolean light) throws Exception {
        T m = dataManager.getMessage(token , messageId, light);
        return m;
    }

    public List<MessageLVO> getMessageInFolder(String token, String folderId, boolean light) throws Exception {
        List<MessageLVO> mm = dataManager.getMessageInFolder(token, folderId, light);
        return mm;
    }

    public MessageLVO delete(String token, String messageId) throws Exception {
        MessageLVO mm = dataManager.deleteMessage(token, messageId);
        return mm;
    }
}
