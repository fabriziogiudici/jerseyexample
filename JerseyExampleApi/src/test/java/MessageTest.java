import org.jerseyexample.api.core.DataManager;
import org.jerseyexample.entity.FolderDO;
import org.jerseyexample.entity.MessageDO;
import org.jerseyexample.entity.UserDO;
import org.jerseyexample.model.MessageLVO;
import org.jerseyexample.model.MessageVO;
import org.jerseyexample.model.UserVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by warez on 24/04/17.
 */
public class MessageTest {

    private DataManager dataManager;

    @Before
    public void init() {
        dataManager = new DataManager();
    }

    @Test
    public void sendMessageTest() throws Exception {
        String email = "marco.rossi@email.it";
        UserVO user = dataManager.login(email, "marco");

        MessageVO message = new MessageVO();
        message.setTo("aleale@email.it" + DataManager.EMAIL_SEPARATOR + "alexViola@email.it");
        message.setOggetto("oggetto");
        message.setBody("testo del messaggio...");

        MessageLVO messageLVO = dataManager.sendMessage(user.getToken(), message);

        UserDO from = dataManager.getUsers().stream().filter(userDO -> userDO.getEmail().equals(email)).findFirst().get();
        UserDO to1 = dataManager.getUsers().stream().filter(userDO -> userDO.getEmail().equals("aleale@email.it")).findFirst().get();
        UserDO to2 = dataManager.getUsers().stream().filter(userDO -> userDO.getEmail().equals("alexViola@email.it")).findFirst().get();

        FolderDO fromInviati = from.getFolders().stream().filter(folderDO -> folderDO.getNome().equals(DataManager.INVIATI)).findFirst().get();
        FolderDO to1Ricevuti = to1.getFolders().stream().filter(folderDO -> folderDO.getNome().equals(DataManager.RICEVUTI)).findFirst().get();
        FolderDO to2Ricevuti = to2.getFolders().stream().filter(folderDO -> folderDO.getNome().equals(DataManager.RICEVUTI)).findFirst().get();

        Assert.assertEquals(fromInviati.getMessages().size(), 1);
        Assert.assertEquals(to1Ricevuti.getMessages().size(), 1);
        Assert.assertEquals(to2Ricevuti.getMessages().size(), 1);

        Assert.assertEquals(fromInviati.getMessages().get(0).getId(), Long.parseLong(messageLVO.getId()));
        Assert.assertEquals(to1Ricevuti.getMessages().get(0).getId(), Long.parseLong(messageLVO.getId()));
        Assert.assertEquals(to2Ricevuti.getMessages().get(0).getId(), Long.parseLong(messageLVO.getId()));
    }

    @Test
    public void deleteMessageTest() throws Exception {
        String email = "marco.rossi@email.it";
        UserVO user = dataManager.login(email, "marco");

        MessageVO message = new MessageVO();
        message.setTo("aleale@email.it" + DataManager.EMAIL_SEPARATOR + "alexViola@email.it");
        message.setOggetto("oggetto");
        message.setBody("testo del messaggio...");

        MessageLVO messageLVO = dataManager.sendMessage(user.getToken(), message);

        UserDO from = dataManager.getUsers().stream().filter(userDO -> userDO.getEmail().equals(email)).findFirst().get();
        FolderDO fromInviati = from.getFolders().stream().filter(folderDO -> folderDO.getNome().equals(DataManager.INVIATI)).findFirst().get();

        Assert.assertEquals(fromInviati.getMessages().size(), 1);
        dataManager.deleteMessage(user.getToken(), messageLVO.getId());
        Assert.assertEquals(fromInviati.getMessages().size(), 0);
    }

    @Test(expected = Exception.class)
    public void deleteInexistentMessageTest() throws Exception {
        String email = "marco.rossi@email.it";
        UserVO user = dataManager.login(email, "marco");

        MessageVO message = new MessageVO();
        message.setTo("aleale@email.it" + DataManager.EMAIL_SEPARATOR + "alexViola@email.it");
        message.setOggetto("oggetto");
        message.setBody("testo del messaggio...");

        dataManager.deleteMessage(user.getToken(), "111");

    }

    @Test
    public void getMessageDetailTest() throws Exception {
        String email = "marco.rossi@email.it";
        UserVO user = dataManager.login(email, "marco");

        MessageVO message = new MessageVO();
        message.setTo("aleale@email.it" + DataManager.EMAIL_SEPARATOR + "alexViola@email.it");
        message.setOggetto("oggetto");
        String body = "testo del messaggio...";
        message.setBody(body);

        MessageLVO messageLVO = dataManager.sendMessage(user.getToken(), message);
        MessageVO messageVO = dataManager.getMessage(user.getToken(), messageLVO.getId(), false);

        Assert.assertEquals(messageVO.getBody(), body);
    }

    @Test
    public void getAllMessageTest() throws Exception {
        String email = "marco.rossi@email.it";
        UserVO userFrom = dataManager.login(email, "marco");

        MessageVO message = new MessageVO();
        String to = "aleale@email.it";
        message.setTo(to);
        message.setOggetto("oggetto");
        String body = "testo del messaggio...";
        message.setBody(body);

        dataManager.sendMessage(userFrom.getToken(), message);
        dataManager.sendMessage(userFrom.getToken(), message);
        dataManager.sendMessage(userFrom.getToken(), message);

        UserVO userTO = dataManager.login(to, "ale");

        List<MessageLVO> messageInFolder1 = dataManager.getMessageInFolder(userTO.getToken(), DataManager.RICEVUTI, true);
        Assert.assertEquals(messageInFolder1.size(), 3);

    }



}
