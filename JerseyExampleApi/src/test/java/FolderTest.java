import org.jerseyexample.api.core.DataManager;
import org.jerseyexample.model.FolderVO;
import org.jerseyexample.model.UserVO;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by warez on 24/04/17.
 */
public class FolderTest {

    private DataManager dataManager;

    @Before
    public void init() {
        dataManager = new DataManager();
    }

    @Test(expected=Exception.class)
    public void createFolderNotLogged() throws Exception {
        dataManager.createFolder("aaaa", "provaFolder");
    }

    @Test(expected=Exception.class)
    public void createFolderWithNullToken() throws Exception {
        dataManager.createFolder(null, "provaFolder");
    }

    @Test
    public void createFolder() throws Exception {
        String email = "marco.rossi@email.it";
        UserVO user = dataManager.login(email, "marco");

        String folderName = "provaFolder";
        dataManager.createFolder(user.getToken(), folderName);

        List<FolderVO> allFolderOf = dataManager.getAllFolderOf(user.getToken());
        for(FolderVO f: allFolderOf)
            if(f.getNome().equals(folderName))
                return;

        throw new Exception("Folder inesistente!");

    }

    @Test(expected=Exception.class)
    public void createExistentFolder() throws Exception{
        String email = "marco.rossi@email.it";
        UserVO user = dataManager.login(email, "marco");

        String folderName = "provaFolder";
        dataManager.createFolder(user.getToken(), folderName);

        dataManager.createFolder(user.getToken(), folderName);
    }

//    @Test
    public void deleteFolder() throws Exception {
        String email = "marco.rossi@email.it";
        UserVO user = dataManager.login(email, "marco");

        String folderName = "provaFolder";
        FolderVO folder = dataManager.createFolder(user.getToken(), folderName);

        dataManager.deleteFolder(user.getToken(), folder.getId());

        List<FolderVO> allFolderOf = dataManager.getAllFolderOf(user.getToken());
        for(FolderVO f: allFolderOf)
            if(f.getNome().equals(folderName))
                throw new Exception("Folder non cancellata.");
    }

    @Test(expected=Exception.class)
    public void deleteNonCancellableFolder() throws Exception {
        String email = "marco.rossi@email.it";
        UserVO user = dataManager.login(email, "marco");

        dataManager.deleteFolder(user.getToken(), DataManager.INVIATI);

    }

}
