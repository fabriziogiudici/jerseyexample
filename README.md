% cd JerseyExampleServer

% mvn tomcat7:run

% curl -v -X POST -H"user: marco.rossi@email.it" -H"password: marco" -H"Accept: application/json"  http://localhost:8080/rest/user/login

{"email":"marco.rossi@email.it","nome":"Marco","cognome":"Rossi","token":"5b7f812a-fc07-4ee8-9b65-104762f5659d"}



% curl -v -H"X-TOKEN: 5b7f812a-fc07-4ee8-9b65-104762f5659d" -H"Accept: application/json" http://localhost:8080/rest/folder

[{"nome":"Inviati","id":"103"},{"nome":"Ricevuti","id":"104"}]


% curl -v -X PUT -H"X-TOKEN: 5b7f812a-fc07-4ee8-9b65-104762f5659d" -H"Accept: application/json" http://localhost:8080/rest/folder/nuova%20cartella
{"nome":"nuova cartella","id":"109"}

% curl -v -H"X-TOKEN: 5b7f812a-fc07-4ee8-9b65-104762f5659d" -H"Accept: application/json" http://localhost:8080/rest/folder

[{"nome":"Inviati","id":"103"},{"nome":"Ricevuti","id":"104"},{"nome":"","id":"109"}]
