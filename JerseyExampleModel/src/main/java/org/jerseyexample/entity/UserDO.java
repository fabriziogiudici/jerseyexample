package org.jerseyexample.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MDONATI on 21/04/2017.
 */
public class UserDO {

    private long id;

    private String email;

    private String password;

    private String nome;

    private String cognome;

    private List<FolderDO> folders = new ArrayList<>();
    private String token;

    public List<FolderDO> getFolders() {
        return folders;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
