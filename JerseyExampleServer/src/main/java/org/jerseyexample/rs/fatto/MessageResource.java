package org.jerseyexample.rs.fatto;

import org.jerseyexample.api.MessageManager;
import org.jerseyexample.model.MessageLVO;
import org.jerseyexample.model.MessageVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by warez on 24/04/17.
 */
@Component
@Path("/message")
public class MessageResource extends AbstractResource {

    private static final Logger log = LoggerFactory.getLogger(MessageResource.class);

    @Autowired
    private MessageManager messageManager;

    @POST
    @PathParam("/send")
    @Consumes("application/json")
    @Produces("application/json")
    public Response sendMessage(MessageVO message) {
        try {

            MessageLVO sentMessage = messageManager.sendMessage( getToken(), message);
            return Response.ok(sentMessage).build();

        } catch(Exception e) {
            log.error(e.getMessage(),e);
            return Response.serverError().entity(e).build();
        }
    }

    @GET
    @PathParam("/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getMessage(@PathParam("id") String id,
                               @QueryParam("listFolder") @DefaultValue("FALSE") boolean listFolder,
                               @QueryParam("type") @DefaultValue("TRUE") boolean light) {
        try {

            if(listFolder) {
                List<MessageLVO> messages = messageManager.getMessageInFolder(getToken() , id, light);
                return Response.ok(messages).build();
            }


            MessageLVO message = messageManager.get(getToken(), id, light);
            return Response.ok(message).build();

        } catch(Exception e) {
            log.error(e.getMessage(),e);
            return Response.serverError().entity(e).build();
        }
    }

    @DELETE
    @PathParam("/{messageId}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response delete(@PathParam("messageId") String messageId) {

        try {

            MessageLVO message = messageManager.delete(getToken() , messageId);
            return Response.ok(message).build();

        } catch(Exception e) {
            log.error(e.getMessage(),e);
            return Response.serverError().entity(e).build();
        }
    }
}
