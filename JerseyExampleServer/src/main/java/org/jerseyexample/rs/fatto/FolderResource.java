package org.jerseyexample.rs.fatto;

import org.jerseyexample.api.FolderManager;
import org.jerseyexample.model.FolderVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by warez on 24/04/17.
 */
@Path("/folder")
public class FolderResource extends AbstractResource {

    private static final Logger log = LoggerFactory.getLogger(FolderResource.class);

    @Autowired
    private FolderManager folderManager;

    @GET
    public Response getList() {
        try {

            List<FolderVO> list = folderManager.getAll( getToken() );
            return Response.ok(list).build();

        } catch(Exception e) {
            log.error(e.getMessage(),e);
            return Response.serverError().entity(e).build();
        }
    }

    @PUT
    @Path("/{folderName}")
    public Response create(@PathParam("folderName") String name) {
        try {

            FolderVO createdFolder = folderManager.create( getToken() , name);
            return Response.ok(createdFolder).build();

        } catch(Exception e) {
            log.error(e.getMessage(),e);
            return Response.serverError().entity(e).build();
        }
    }

    @DELETE
    @Path("/{folderId}")
    public Response delete(@PathParam("folderId") String folderId) {
        try {

            folderManager.delete( getToken(), folderId);
            return Response.ok().build();

        } catch(Exception e) {
            log.error(e.getMessage(),e);
            return Response.serverError().entity(e).build();
        }
    }

}
