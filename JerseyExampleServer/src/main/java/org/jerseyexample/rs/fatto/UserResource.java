package org.jerseyexample.rs.fatto;

import org.jerseyexample.api.UserManager;
import org.jerseyexample.model.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Created by warez on 24/04/17.
 */
@Component
@Path("/user")
public class UserResource extends AbstractResource{

    private static final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Autowired
    private UserManager userManager;

    @POST
    @Path("/login")
    public Response login(@HeaderParam("user") String user, @HeaderParam("password") String password) {

        try {

            UserVO userVO = userManager.login(user, password);
            return Response.ok(userVO).build();

        } catch(Exception e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e).build();
        }

    }

    @POST
    @Path("/logout")
    public Response logout() {

        try {

            UserVO user = userManager.logout( getToken() );
            return Response.ok(user).build();

        } catch(Exception e) {
            log.error(e.getMessage(), e);
            return Response.serverError().entity(e).build();
        }
    }

}
