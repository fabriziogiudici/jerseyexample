package org.jerseyexample.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;

/**
 * Created by warez on 25/04/17.
 */
public class SwaggerBootstrap extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(SwaggerBootstrap.class);

    @Override
    public void init(ServletConfig servletConfig) {
        try {

            ServletContext sc = servletConfig.getServletContext();

            //as of servlet api 2.5
            String ctxPath = "rest";
            String apiversion = "1.0";
            String hostname = "localhost";

            /*ConfigFactory.config().setBasePath("http://" + hostname + ":8080" + ctxPath);
            ConfigFactory.config().setApiPath("http://" + hostname + ":8080" + ctxPath);
            ConfigFactory.config().setApiVersion(apiversion);
            ConfigFactory.config().setSwaggerVersion(com.wordnik.swagger.core.SwaggerSpec.version());*/


            log.info("Swagger:");
            log.info("api hostname:" + hostname);
            log.info("context path:" + ctxPath);
            log.info("api-version:" + apiversion);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
